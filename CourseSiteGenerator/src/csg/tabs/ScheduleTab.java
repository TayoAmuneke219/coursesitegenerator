/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.tabs;

import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import csg.data.Schedule;
import csg.data.Schedule;
import csg.data.ScheduleData;
import csg.workspace.ScheduleController;
import java.time.LocalDate;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import properties_manager.PropertiesManager;

/**
 *
 * @author jastworld
 */
public class ScheduleTab extends Tab{
        //App to be used in another call
    CourseSiteGeneratorApp app;
    
    //VBox to hold the entire pane
    VBox schedule;
    
    ScheduleController controller;
    
    //All the components that need to be outside
    ComboBox typeBox;
    DatePicker datePicker;
    TextField timeField;
    TextField titleField;
    TextField topicField;
    TextField linkField;
    TextField criteriaField;
    
    TableView<Schedule> table;
    
    PropertiesManager props;


    
    public ScheduleTab(CourseSiteGeneratorApp initApp){
        app= initApp;
        //Disable close capabilities
        
        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        props = PropertiesManager.getPropertiesManager();
        //String tasHeaderText = props.getProperty(CourseSiteGeneratorProp.SCHEDULE_DATA.toString());
        
        setClosable(false);
        schedule = new VBox();
        schedule.setPadding(new Insets(20, 10, 20, 10));
        schedule.setSpacing(10);
        setText(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_DATA.toString()));
        Text labelTop = new Text(props.getProperty(CourseSiteGeneratorProp.SCHEDULE.toString()));
        labelTop.setFont(Font.font("Arial", FontWeight.NORMAL,20));
        schedule.getChildren().add(labelTop);
        
        //Vox 2
        GridPane calPicker = new GridPane();
        Text calBoundaries = new Text(props.getProperty(CourseSiteGeneratorProp.CALENDAR_BOUNDARIES.toString()));
        calBoundaries.setFont(Font.font("Arial", FontWeight.NORMAL,15));
        calPicker.add(calBoundaries, 0, 0);
        
        // Create the DatePicker.
        DatePicker startDatePicker = new DatePicker();
        startDatePicker.setValue(LocalDate.now());
        // Add some action (in Java 8 lambda syntax style).
        startDatePicker.setOnAction(event -> {
            LocalDate date = startDatePicker.getValue();
            System.out.println("Selected start date: " + date);
        });
        
        // Create the DatePicker.
        DatePicker endDatePicker = new DatePicker();
        endDatePicker.setValue(LocalDate.now());
        // Add some action (in Java 8 lambda syntax style).
        endDatePicker.setOnAction(event -> {
            LocalDate date = endDatePicker.getValue();
            System.out.println("Selected  end date: " + date);
        });        
        calPicker.add(new Label(props.getProperty(CourseSiteGeneratorProp.STARTING.toString())),0,1);
        calPicker.add(startDatePicker, 1, 1,3,1);
        
        calPicker.add(new Label(props.getProperty(CourseSiteGeneratorProp.ENDING.toString())),8,1);
        calPicker.add(endDatePicker, 9, 1,3,1);
        calPicker.setStyle("-fx-border-color: black; -fx-padding: 10;-fx-background-color: white");
        schedule.getChildren().add(calPicker);
        
        
        VBox scheduleItems = new VBox();
        //
        HBox topScheduleStuff = new HBox();
        Text scheduleText = new Text(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_ITEMS.toString()));
        scheduleText.setFont(Font.font("Arial", FontWeight.NORMAL,15));
        Button dash = new Button(props.getProperty(CourseSiteGeneratorProp.DASH.toString()));
        dash.setOnAction(e->{
            controller.handleDeleteRow(this);
        });
        
        topScheduleStuff.getChildren().addAll(scheduleText,dash);
        scheduleItems.getChildren().add(topScheduleStuff);
        
               
        //Add up the table
               //Table
        table = new TableView<>();
        
        // Create column Use
        TableColumn<Schedule, String> typeCol //
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.TYPE.toString()));
        // Create column NavBarTitle (Data type of String)
        TableColumn<Schedule, String> dateCol//
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.DATE.toString()));
        TableColumn<Schedule, String> titleCol//
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.TITLE.toString()));
        
        TableColumn<Schedule, String> topicCol//
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.TOPIC.toString()));

        typeCol.setCellValueFactory(new PropertyValueFactory<>("type"));
        dateCol.setCellValueFactory(new PropertyValueFactory<>("date"));
        titleCol.setCellValueFactory(new PropertyValueFactory<>("title"));
        topicCol.setCellValueFactory(new PropertyValueFactory<>("topic"));
        
        ScheduleData data = (ScheduleData) app.getScheduleData();
        ObservableList<Schedule> tableData = data.getScheduleList();
        table.setItems(tableData);
        
        
        table.getColumns().addAll(typeCol,dateCol,titleCol,topicCol);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.autosize();
        
        //INSTANTIATES A CONTROLLER FOR THE TABLE
        controller = new ScheduleController(app);
        
        scheduleItems.getChildren().add(table);
        
                
        //The Add/Edit type
        GridPane addEdit = new GridPane();
        //Check later
        addEdit.setMinWidth(5);
        
        scheduleItems.setStyle("-fx-border-color: black; -fx-padding: 10;-fx-background-color: white");
        addEdit.add(new Text(props.getProperty(CourseSiteGeneratorProp.ADD_EDIT.toString())), 0, 0);
        
        //Line 1
        Label type = new Label(props.getProperty(CourseSiteGeneratorProp.TYPE.toString())+props.getProperty(CourseSiteGeneratorProp.COLON.toString()));
        typeBox = new ComboBox();
        typeBox.getItems().addAll(props.getProperty(CourseSiteGeneratorProp.HOLIDAY.toString()), props.getProperty(CourseSiteGeneratorProp.LECTURE.toString()),props.getProperty(CourseSiteGeneratorProp.HW.toString()));
        
        typeBox.setValue(props.getProperty(CourseSiteGeneratorProp.HOLIDAY.toString()));
        typeBox.setOnAction(e->{
            controller.handleAddSchedule(this);
        });
        
        addEdit.add(type, 0, 1);
        addEdit.add(typeBox, 1, 1,3,1);
        
        //Line 2
        Label date = new Label(props.getProperty(CourseSiteGeneratorProp.DATE.toString())+props.getProperty(CourseSiteGeneratorProp.COLON.toString()));
        // Create the DatePicker.
        datePicker = new DatePicker();
        datePicker.setValue(LocalDate.now());
        // Add some action (in Java 8 lambda syntax style).
        datePicker.setOnAction(event -> {
            LocalDate date2 = datePicker.getValue();
            System.out.println("Selected start date: " + date);
        });
        addEdit.add(date, 0, 2);
        addEdit.add(datePicker, 1, 2,3,1);
        
        //Time
        Label time = new Label(props.getProperty(CourseSiteGeneratorProp.TIME.toString())+props.getProperty(CourseSiteGeneratorProp.COLON.toString())+"\t");
        timeField = new TextField();
        //dayTimeField.setText("Mondays 3:30pm-4:23pm");
        timeField.setOnAction(e->{
            controller.handleAddSchedule(this);
        });
        addEdit.add(time, 0, 3);
        addEdit.add(timeField, 1, 3,3,1);
        
        
        Label title = new Label(props.getProperty(CourseSiteGeneratorProp.TITLE.toString())+props.getProperty(CourseSiteGeneratorProp.COLON.toString()));
        titleField = new TextField();
        titleField.setText(props.getProperty(CourseSiteGeneratorProp.SNOW_DAY.toString()));
        titleField.setOnAction(e->{
            controller.handleAddSchedule(this);
        });
        addEdit.add(title, 0, 4);
        addEdit.add(titleField, 1, 4,3,1);

        Label topic2 = new Label(props.getProperty(CourseSiteGeneratorProp.TOPIC.toString())+":");
        topicField = new TextField();
        topicField.setOnAction(e->{
            controller.handleAddSchedule(this);
        });
        addEdit.add(topic2, 0, 5);
        addEdit.add(topicField, 1, 5,3,1);
        
        Label link = new Label(props.getProperty(CourseSiteGeneratorProp.LINK.toString())+"");
        linkField = new TextField();
        linkField.setOnAction(e->{
            controller.handleAddSchedule(this);
        });
        linkField.setText("http://funnybizblog.com/funny-stuff/calvin-hobies-snowman-cartoons");
        addEdit.add(link, 0, 6);
        addEdit.add(linkField, 1, 6,3,1);

        Label criteria = new Label(props.getProperty(CourseSiteGeneratorProp.CRITERIA.toString()));
        criteriaField = new TextField();
        criteriaField.setOnAction(e->{
            controller.handleAddSchedule(this);
        });
        addEdit.add(criteria, 0, 7);
        addEdit.add(criteriaField, 1, 7,3,1);

        Button addUpdate = new Button();
        addUpdate.setOnAction(e->{
            controller.handleAddSchedule(this);
        });
        addUpdate.setText(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON.toString()));
        
        Button clear = new Button();
        clear.setText(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON.toString()));
        clear.setOnAction(e->{
            resetFields();
            
        });
        addEdit.add(addUpdate, 0, 8);
        addEdit.add(clear, 1, 8,1,1);
        scheduleItems.getChildren().add(addEdit);
        
        
        schedule.getChildren().add(scheduleItems);
        
        
        
        
        schedule.setStyle("-fx-background-color: #3B5998;  -fx-font-family: Courier New;");

        setContent(schedule);
    
    }

    public void resetFields() {
        getTypeBox().setValue(props.getProperty(CourseSiteGeneratorProp.HOLIDAY.toString()));
        getDatePicker().setValue(LocalDate.now());
        getTimeField().clear();
        getTitleField().clear();
        getTopicField().clear();
        getLinkField().clear();
        getCriteriaField().clear();
        
    }
    public ComboBox getTypeBox() {
        return typeBox;
    }

    public DatePicker getDatePicker() {
        return datePicker;
    }

    public TextField getTimeField() {
        return timeField;
    }

    public TextField getTitleField() {
        return titleField;
    }

    public TextField getTopicField() {
        return topicField;
    }

    public TextField getLinkField() {
        return linkField;
    }

    public TextField getCriteriaField() {
        return criteriaField;
    }
    
    public TableView<Schedule> getTable() {
        return table;
    }
    
}
