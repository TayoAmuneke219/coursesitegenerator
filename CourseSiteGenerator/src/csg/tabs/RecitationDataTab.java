/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.tabs;

import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import csg.data.Pages;
import csg.data.Recitation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;

import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import properties_manager.PropertiesManager;

/**
 *
 * @author jastworld
 */
public class RecitationDataTab extends Tab{
    
    //App to be used in another call
    CourseSiteGeneratorApp app;
    
    //VBox to hold the entire pane
    VBox recitation;
    PropertiesManager props;
    
    TableView<Recitation> table;
    private final TextField instructorField;
    private final TextField sectionField;
    private final TextField locationField;
    private final TextField dayTimeField;
    
    public RecitationDataTab(CourseSiteGeneratorApp initApp){
        app= initApp;
        recitation = new VBox();
        recitation.setPadding(new Insets(20, 10, 20, 10));
        props = PropertiesManager.getPropertiesManager();
        
        setText(props.getProperty(CourseSiteGeneratorProp.RECITATION_DATA.toString()));
        Text label = new Text(props.getProperty(CourseSiteGeneratorProp.RECITATIONS.toString()));
        label.setFont(Font.font("Arial", FontWeight.NORMAL,20));
        
        Button dash = new Button(props.getProperty(CourseSiteGeneratorProp.DASH.toString()));
        FlowPane top = new FlowPane();
        top.setStyle("-fx-border-color: black; -fx-padding: 10;-fx-background-color: white");
        top.getChildren().addAll(label,dash);
        
        recitation.getChildren().add(top);
        
        
        //Table
        table = new TableView<>();
        // Create column Use
        TableColumn<Recitation, String> sectionCol //
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.SECTION.toString()));
        // Create column NavBarTitle (Data type of String)
        TableColumn<Recitation, String> instructorCol//
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.INSTRUCTOR.toString()));
        TableColumn<Recitation, String> locationCol//
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.LOCATION.toString()));
        
        TableColumn<Recitation, String> dayTimeCol//
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.DAY_TIME.toString()));

        TableColumn<Recitation, String> ta1Col//
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.TA.toString()));
        
        TableColumn<Recitation, String> ta2Col//
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.TA.toString()));
        
        
        
        sectionCol.setCellValueFactory(new PropertyValueFactory<>("section"));
        instructorCol.setCellValueFactory(new PropertyValueFactory<>("instructor"));
        locationCol.setCellValueFactory(new PropertyValueFactory<>("location"));
        dayTimeCol.setCellValueFactory(new PropertyValueFactory<>("dayTime"));
        ta1Col.setCellValueFactory(new PropertyValueFactory<>("ta"));
        // Display row data
        ta2Col.setCellValueFactory(new PropertyValueFactory<>("ta2"));
        
        ObservableList<Recitation> list = getRecitationList();
        table.setItems(list);
        table.getColumns().addAll(sectionCol,instructorCol,dayTimeCol,locationCol,ta1Col,ta2Col);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.autosize();
        table.setStyle("-fx-border-color: black; -fx-padding: 10;-fx-background-color: white");
        recitation.getChildren().add(table);

                
        //The Add/Edit section
        GridPane addEdit = new GridPane();
        addEdit.setStyle("-fx-border-color: black; -fx-padding: 10;-fx-background-color: white");
        addEdit.add(new Text(props.getProperty(CourseSiteGeneratorProp.ADD_EDIT.toString())), 0, 0);
        
        Label section = new Label(props.getProperty(CourseSiteGeneratorProp.SECTION.toString())+props.getProperty(CourseSiteGeneratorProp.COLON.toString()));
        sectionField = new TextField();
        sectionField.setText(props.getProperty(CourseSiteGeneratorProp.R01.toString()));
        addEdit.add(section, 0, 1);
        addEdit.add(sectionField, 1, 1,3,1);
        
        
        Label instructor = new Label(props.getProperty(CourseSiteGeneratorProp.INSTRUCTOR.toString())+":\t");
        instructorField = new TextField();
        instructorField.setText(props.getProperty(CourseSiteGeneratorProp.MCKENNA.toString()));
        addEdit.add(instructor, 0, 2);
        addEdit.add(instructorField, 1, 2,3,1);
        
        //Day Time
        Label dayTime = new Label(props.getProperty(CourseSiteGeneratorProp.DAY_TIME.toString()));
        dayTimeField = new TextField();
        dayTimeField.setText(props.getProperty(CourseSiteGeneratorProp.DT.toString()));
        addEdit.add(dayTime, 0, 3);
        addEdit.add(dayTimeField, 1, 3,3,1);
        
        
        Label location = new Label(props.getProperty(CourseSiteGeneratorProp.LOCATION.toString()));
        locationField = new TextField();
        locationField.setText(props.getProperty(CourseSiteGeneratorProp.ROOM_EX.toString()));
        addEdit.add(location, 0, 4);
        addEdit.add(locationField, 1, 4,3,1);

        Label ta1 = new Label(props.getProperty(CourseSiteGeneratorProp.SUPERVISING_TA.toString()));
        ComboBox ta1Box = new ComboBox();
        ta1Box.getItems().addAll(props.getProperty(CourseSiteGeneratorProp.TA_1.toString()),props.getProperty(CourseSiteGeneratorProp.TA_2.toString()));
        ta1Box.setValue(props.getProperty(CourseSiteGeneratorProp.TA_1.toString()));
        addEdit.add(ta1, 0, 5);
        addEdit.add(ta1Box, 1, 5,3,1);

        Label ta2 = new Label(props.getProperty(CourseSiteGeneratorProp.SECTION.toString()));
        ComboBox ta2Box = new ComboBox();
        ta2Box.getItems().addAll(props.getProperty(CourseSiteGeneratorProp.TA_2.toString()),props.getProperty(CourseSiteGeneratorProp.TA_1.toString()));
        ta2Box.setValue(props.getProperty(CourseSiteGeneratorProp.TA_2.toString()));
        addEdit.add(ta2, 0, 6);
        addEdit.add(ta2Box, 1, 6,3,1);

        Button addUpdate = new Button();
        addUpdate.setText(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON.toString()));
        
        Button clear = new Button();
        clear.setOnAction(e->{
            resetWorkspace();
        });
        clear.setText(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON.toString()));
        addEdit.add(addUpdate, 0, 7);
        addEdit.add(clear, 1, 7,1,1);
        recitation.getChildren().add(addEdit);
        
        
        recitation.setSpacing(10);
        recitation.setStyle("-fx-background-color: #3B5998;  -fx-font-family: Courier New;");
        
        
        //Hook it up to recitation
        setContent(recitation);
    
    }
    
    private ObservableList<Recitation> getRecitationList() {
        
        Recitation r01 = new Recitation("R02", "McKenna","Wed 3:30pm-4:23pm", "Old CS 2114",
           "Jane Doe","Joe Shmo");
 
        Recitation r02 = new Recitation("R05", "Banerjee","Tue 5:30pm-6:23pm", "Old CS 2114",
           "","");
        
        ObservableList<Recitation> list = FXCollections.observableArrayList(r01,r02);
        return list;
    }
    
    public void resetWorkspace(){
        table.refresh();
        instructorField.clear();
        sectionField.clear();
        locationField.clear();
        dayTimeField.clear();
    
    }
    
    
    
}
