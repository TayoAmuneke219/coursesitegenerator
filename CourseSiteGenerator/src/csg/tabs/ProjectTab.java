/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.tabs;

import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import csg.data.ProjectData;
import csg.data.Schedule;
import csg.data.ScheduleData;
import csg.data.Students;
import csg.data.Teams;
import csg.workspace.ProjectController;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import properties_manager.PropertiesManager;

/**
 *
 * @author Tayo Amuneke
 */
public class ProjectTab extends Tab{
    
        //App to be used in another call
    CourseSiteGeneratorApp app;
    
    //Controller for the controls
    ProjectController controller;
    
    //VBox to hold the entire pane
    VBox project;
    
    TableView<Teams> teamTable;
    TableView<Students> studentTable;
    
    //Accessible nodes
    TextField nameField;
    ColorPicker colorCPicker;
    ColorPicker colorTPicker;
    TextField fNameField;
    private TextField lNameField;
    private ComboBox teamChoiceBox;
    private TextField roleField;
    private TextField linkField;

    
    
    PropertiesManager props;
    public ProjectTab(CourseSiteGeneratorApp initApp){
        app= initApp;
        setClosable(false);
        controller = new ProjectController(app);
        project = new VBox();
        props = PropertiesManager.getPropertiesManager();
        project.setPadding(new Insets(20, 10, 20, 10));
        project.setSpacing(10);
        setText(props.getProperty(CourseSiteGeneratorProp.PROJECTS_DATA.toString()));
        
        Text label = new Text(props.getProperty(CourseSiteGeneratorProp.PROJECTS.toString()));
        label.setFont(Font.font("Arial", FontWeight.NORMAL,20));
        project.getChildren().add(label);
        
        
        //VBox 2 to hold all other components
        VBox teamBox = new VBox();
        HBox topteamStuff = new HBox();
        Text teamText = new Text(props.getProperty(CourseSiteGeneratorProp.TEAMS.toString()));
        teamText.setFont(Font.font("Arial", FontWeight.NORMAL,15));
        Button dash = new Button(props.getProperty(CourseSiteGeneratorProp.DASH.toString()));
        dash.setOnAction(e->{
            controller.handleDeleteTeam(this);
        });
        
        topteamStuff.getChildren().addAll(teamText,dash);
         teamBox.getChildren().add(topteamStuff);
        
        //
        teamTable = new TableView<>();
                // Create column Use
        TableColumn<Teams, String> nameCol //
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.NAME.toString()));
        // Create column NavBarTitle (Data type of String)
        TableColumn<Teams, String> colorCol//
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.COLOR_HEX.toString()));
        TableColumn<Teams, String> textColorCol//
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.TEXT_COLOR_HEX.toString()));
        
        TableColumn<Teams, String> linkCol//
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.LINK.toString()));

        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        colorCol.setCellValueFactory(new PropertyValueFactory<>("color"));
        textColorCol.setCellValueFactory(new PropertyValueFactory<>("textColor"));
        linkCol.setCellValueFactory(new PropertyValueFactory<>("link"));
        
        ProjectData data = (ProjectData) app.getProjectData();
        ObservableList<Teams> tableData = data.getTeamList();
        teamTable.setItems(tableData);
        
        
        teamTable.getColumns().addAll(nameCol,colorCol,textColorCol,linkCol);
        teamTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        teamTable.autosize();
        teamBox.getChildren().add(teamTable);
        
        GridPane addEditTeams = new GridPane();
        Label name = new Label(props.getProperty(CourseSiteGeneratorProp.NAME.toString()));
        nameField = new TextField();
        nameField.setText(props.getProperty(CourseSiteGeneratorProp.C4_COMICS.toString()));
        nameField.setOnAction(e->{
            controller.handleAddTeam(this);
        });
        addEditTeams.add(new Label(props.getProperty(CourseSiteGeneratorProp.ADD_EDIT.toString())), 0, 0);
        addEditTeams.add(name, 0, 1);
        addEditTeams.add(nameField, 1, 1,3,1);
        
        HBox colorado = new HBox();
        Label colorC= new Label(props.getProperty(CourseSiteGeneratorProp.COLOR.toString())+": \t");
        colorCPicker = new ColorPicker();
        colorCPicker.setOnAction(e -> {
            controller.handleAddTeam(this);
         });
        Label colorT= new Label(props.getProperty(CourseSiteGeneratorProp.TEXT_COLOR.toString())+": \t");
        colorTPicker = new ColorPicker();
        colorTPicker.setOnAction(e -> {
            controller.handleAddTeam(this);
         });
        colorado.getChildren().addAll(colorC,colorCPicker,colorT,colorTPicker);
        addEditTeams.add(colorado,0,2,7,1);
        
        linkField = new TextField();
        linkField.setOnAction(e->{
            controller.handleAddTeam(this);
        });
        addEditTeams.add(new Label(props.getProperty(CourseSiteGeneratorProp.LINK.toString())+" \t"), 0, 3);
        
        addEditTeams.add(linkField,1,3,5,1);
        
        Button addUpdateTeam = new Button();
        addUpdateTeam.setOnAction(e->{
            controller.handleAddTeam(this);
        });
        addUpdateTeam.setText(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON.toString()));
        
        Button clearTeam = new Button();
        clearTeam.setText(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON.toString()));
        clearTeam.setOnAction(e->{
            resetTeamFields();
            
        });
        
        addEditTeams.add(addUpdateTeam, 0, 4);
        addEditTeams.add(clearTeam, 1, 4);
        
        
        teamBox.getChildren().add(addEditTeams);
        teamBox.setStyle("-fx-border-color: black; -fx-padding: 10;-fx-background-color: white");
        project.getChildren().add(teamBox);
        ////////////////////////////////////////////
        
        
        VBox studentBox = new VBox();
        
        HBox topStudentStuff = new HBox();
        Text studentText = new Text(props.getProperty(CourseSiteGeneratorProp.STUDENTS.toString()));
        studentText.setFont(Font.font("Arial", FontWeight.NORMAL,15));
        Button dash1 = new Button(props.getProperty(CourseSiteGeneratorProp.DASH.toString()));
        dash1.setOnAction(e->{
            controller.handleDeleteStudent(this);
        });
        
        topStudentStuff.getChildren().addAll(studentText,dash1);
        studentBox.getChildren().add(topStudentStuff);
        
        //
        studentTable = new TableView<>();
                // Create column Use
        TableColumn<Students, String> firstNameCol //
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.FNAME.toString()));
        // Create column NavBarTitle (Data type of String)
        TableColumn<Students, String> lastNameCol//
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.LNAME.toString()));
        TableColumn<Students, String> teamCol//
              = new TableColumn(props.getProperty(CourseSiteGeneratorProp.TEAM.toString()));
        
        TableColumn<Students, String> roleCol//
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.ROLE.toString()));

        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        teamCol.setCellValueFactory(new PropertyValueFactory<>("team"));
        roleCol.setCellValueFactory(new PropertyValueFactory<>("role"));
        
        ProjectData studentData = (ProjectData) app.getProjectData();
        ObservableList<Students> studentList = studentData.getStudents();
        studentTable.setItems(studentList);
        
        
        studentTable.getColumns().addAll(firstNameCol,lastNameCol,teamCol,roleCol);
        studentTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        studentTable.autosize();
        studentBox.getChildren().add(studentTable);
        
        studentBox.getChildren().add(new Label(props.getProperty(CourseSiteGeneratorProp.ADD_EDIT.toString())));
        
        GridPane studentInfo = new GridPane();
        Label fName = new Label(props.getProperty(CourseSiteGeneratorProp.FNAME.toString()));
        fNameField = new TextField();
        fNameField.setText("Tayo Amuneke");
        fNameField.setOnAction(e->{
            controller.handleAddStudent(this);
        });
        studentInfo.add(fName, 0, 0);
        studentInfo.add(fNameField, 1, 0,3,1);

        Label lName = new Label(props.getProperty(CourseSiteGeneratorProp.LNAME.toString())+":\t");
        lNameField= new TextField();
        lNameField.setOnAction(e->{
            controller.handleAddStudent(this);
        });
        studentInfo.add(lName, 0, 1);
        studentInfo.add(lNameField, 1, 1,3,1);

        
        Label teamName = new Label(props.getProperty(CourseSiteGeneratorProp.TEAM.toString())+":\t");
        teamChoiceBox = new ComboBox();
        
        teamChoiceBox.setItems(tableData);
        teamChoiceBox.setOnAction(e->{
            controller.handleAddStudent(this);
        });
        
        studentInfo.add(teamName, 0, 2);
        studentInfo.add(teamChoiceBox, 1, 2,3,1);

        Label roleLabel = new Label(props.getProperty(CourseSiteGeneratorProp.ROLE.toString())+":\t");
        roleField = new TextField();
        roleField.setOnAction(e->{
            controller.handleAddStudent(this);
        });
        studentInfo.add(roleLabel, 0, 3);
        studentInfo.add(roleField, 1, 3,3,1);

        Button addUpdateStudent = new Button();
        addUpdateStudent.setOnAction(e->{
            controller.handleAddStudent(this);
        });
        addUpdateStudent.setText(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON.toString()));
        
        Button clearStudent = new Button();
        clearStudent.setText(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON.toString()));
        clearStudent.setOnAction(e->{
            resetStudentFields();
            
        });
        studentInfo.add(addUpdateStudent, 0, 4);
        studentInfo.add(clearStudent, 1, 4,1,1);
        
        
        studentBox.getChildren().add(studentInfo);
        studentBox.setStyle("-fx-border-color: black; -fx-padding: 10;-fx-background-color: white");
        project.getChildren().add(studentBox);
        project.setStyle("-fx-background-color: #3B5998; -fx-font-family: Courier New;");
        
        ScrollPane output = new ScrollPane(project);
        output.setFitToWidth(true);
        
        setContent(output);
    
    }

    private void resetTeamFields() {
        nameField.clear();
        colorCPicker.setValue(Color.RED);
        colorTPicker.setValue(Color.BLUE);
        linkField.clear();
    }

    private void resetStudentFields() {
        fNameField.clear();
        lNameField.clear();
        teamChoiceBox.setValue(props.getProperty(CourseSiteGeneratorProp.C4_COMICS.toString()));
        roleField.clear();

    }
    
    public TableView<Teams> getTeamTable() {
        return teamTable;
    }

    public TableView<Students> getStudentTable() {
        return studentTable;
    }

    public TextField getNameField() {
        return nameField;
    }

    public ColorPicker getColorCPicker() {
        return colorCPicker;
    }

    public ColorPicker getColorTPicker() {
        return colorTPicker;
    }

    public TextField getfNameField() {
        return fNameField;
    }

    public TextField getlNameField() {
        return lNameField;
    }

    public ComboBox getTeamChoiceBox() {
        return teamChoiceBox;
    }

    public TextField getRoleField() {
        return roleField;
    }

    public TextField getLinkField() {
        return linkField;
    }

    public void resetWorkspace() {
        resetStudentFields();
        resetTeamFields();
    }
}
