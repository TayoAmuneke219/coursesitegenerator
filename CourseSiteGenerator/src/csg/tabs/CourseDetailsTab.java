/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.tabs;

import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import csg.data.Pages;
import djf.components.AppDataComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import properties_manager.PropertiesManager;
import javafx.scene.text.Text;

/**
 *
 * @author Tayo Amuneke
 */
public class CourseDetailsTab extends Tab{
    
    CourseSiteGeneratorApp app;
    
    //First VBox for the title
    GridPane courseInfoPane;
    Text courseInfoTitle;
    
    PropertiesManager props;
    private TextField courseTitleText;
    private final TextField instructorNameField;
    private final TextField instructorHomeField;
    
    
    //Constructor for our Detail Tab
    public CourseDetailsTab(CourseSiteGeneratorApp initApp){
        app=initApp;
        props = PropertiesManager.getPropertiesManager();
        setText(props.getProperty(CourseSiteGeneratorProp.COURSE_DETAILS.toString()));
        VBox csgDetailLayout = new VBox();
        csgDetailLayout.setPadding(new Insets(20, 10, 20, 10));
        
        //GridPane for Box 1
        courseInfoPane =  new GridPane();
        
        //The first VBox with course Info
        courseInfoTitle = new Text(props.getProperty(CourseSiteGeneratorProp.COURSE_INFO.toString()));
        courseInfoTitle.setFont(Font.font("Arial", FontWeight.NORMAL,15));
        courseInfoPane.add(courseInfoTitle, 0, 0);
        
        //Line 1
        Label subject =  new Label(props.getProperty(CourseSiteGeneratorProp.SUBJECT.toString()));
        
        ComboBox<String> subjectBox = new ComboBox<String>();
        subjectBox.getItems().addAll(props.getProperty(CourseSiteGeneratorProp.CSE.toString()),props.getProperty(CourseSiteGeneratorProp.ISE.toString()));
        subjectBox.setValue(props.getProperty(CourseSiteGeneratorProp.CSE.toString()));
        Label number =  new Label();
        number.setText(props.getProperty(CourseSiteGeneratorProp.NUMBER.toString()));
        ComboBox<String> numberBox = new ComboBox<String>();
        numberBox.getItems().addAll(props.getProperty(CourseSiteGeneratorProp.DEMO219.toString()),"308","312","214","380","381");
        numberBox.setValue(props.getProperty(CourseSiteGeneratorProp.DEMO219.toString()));
        //Hook them to the HBox
        courseInfoPane.add(subject, 0, 1);
        courseInfoPane.add(subjectBox, 1, 1);
        
        
        courseInfoPane.add(number, 3, 1);
        courseInfoPane.add(numberBox, 4, 1);
        
        
        
        //Line 2
        Label semesterLabel =  new Label();
        semesterLabel.setText(props.getProperty(CourseSiteGeneratorProp.SEMESTER.toString()));
        ComboBox<String> semesterBox = new ComboBox<String>();
        semesterBox.getItems().addAll(props.getProperty(CourseSiteGeneratorProp.FALL.toString()),props.getProperty(CourseSiteGeneratorProp.SPRING.toString()));
        semesterBox.setValue(props.getProperty(CourseSiteGeneratorProp.FALL.toString()));
        Label year =  new Label();
        year.setText(props.getProperty(CourseSiteGeneratorProp.YEAR.toString()));
        ComboBox<String> yearBox = new ComboBox<String>();
        yearBox.getItems().addAll("2017","2018","2019","2020","2021");
        yearBox.setValue("2017");
        //Hook them to the HBox
        courseInfoPane.add(semesterLabel, 0, 2);
        courseInfoPane.add(semesterBox, 1, 2);
        courseInfoPane.add(year, 3, 2);
        courseInfoPane.add(yearBox, 4, 2);

        
        
        //Line 3
        Label courseTitle =  new Label(props.getProperty(CourseSiteGeneratorProp.TITLE.toString()));
        courseTitleText = new TextField();
        courseTitleText.setText(props.getProperty(CourseSiteGeneratorProp.TITLE_TEXT.toString()));
        
        courseInfoPane.add(courseTitle, 0, 3);
        courseInfoPane.add(courseTitleText, 1, 3,4,1);
        

        //Line 4
        Label instructorName =  new Label(props.getProperty(CourseSiteGeneratorProp.INSTRUCTOR_NAME.toString()));
        instructorNameField = new TextField(props.getProperty(CourseSiteGeneratorProp.INSTRUCTOR_NAME_TEXT.toString()));
        courseInfoPane.add(instructorName, 0, 4);
        courseInfoPane.add(instructorNameField, 1, 4,4,1);
                //Line 5
        Label instructorHome =  new Label(props.getProperty(CourseSiteGeneratorProp.INSTRUCTOR_HOME.toString()));
        instructorHomeField = new TextField(props.getProperty(CourseSiteGeneratorProp.INSTRUCTOR_HOME_TEXT.toString()));
        courseInfoPane.add(instructorHome, 0, 5);
        courseInfoPane.add(instructorHomeField, 1, 5,4,1);
        
        //Line 6
        //Line 6
        Label exportDir =  new Label(props.getProperty(CourseSiteGeneratorProp.EXPORT_DIR.toString()));
        Label  exportDirTxt= new Label(props.getProperty(CourseSiteGeneratorProp.EXPORT_DIR_TEXT.toString()));
        Button changeDir = new Button(props.getProperty(CourseSiteGeneratorProp.CHANGE.toString()));
        changeDir.setOnAction(e->{
                //Implement later
                //SET THE TEXT TO THE CURRENT DIR
        });
        courseInfoPane.add(exportDir, 0, 6);
        courseInfoPane.add(exportDirTxt, 1, 6);
        courseInfoPane.add(changeDir, 4, 6);
        courseInfoPane.setStyle("-fx-border-color: black; -fx-padding: 10;-fx-background-color: white");
        //courseInfoPane.setStyle("-fx-padding: 10");
        courseInfoPane.setHgap(10);
        
        
        //VBOX 2
        VBox siteTemplate = new VBox();
        Text templateSectionHeader = new Text(props.getProperty(CourseSiteGeneratorProp.SITE_TEMPLATE.toString()));
        templateSectionHeader.setFont(Font.font("Arial", FontWeight.NORMAL,15));
        Label templateSectionText = new Label(props.getProperty(CourseSiteGeneratorProp.SITE_TEMPLATE_MESSAGE.toString()));
        Label templateDir = new Label(props.getProperty(CourseSiteGeneratorProp.SITE_TEMPLATE_DIR.toString()));
        Button templateButton = new Button(props.getProperty(CourseSiteGeneratorProp.SITE_TEMPLATE_BUTTON.toString()));
       
        Label sitePages = new Label(props.getProperty(CourseSiteGeneratorProp.SITE_PAGES.toString()));
        
        
        TableView<Pages> table = new TableView<>();
        // Create column Use
        TableColumn<Pages, CheckBox> useCol //
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.USE.toString()));
        // Create column NavBarTitle (Data type of String)
        TableColumn<Pages, String> navbarTitleCol//
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.NAVBAR_TITLE.toString()));
        // Create column FullName (Data type of String).
        TableColumn<Pages, String> fileNameCol//
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.FILE_NAME.toString()));
        // Create column for script.
        TableColumn<Pages, String> scriptCol//
              = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.SCRIPT.toString()));
        useCol.setCellValueFactory(new PropertyValueFactory<>("checkBox"));
        navbarTitleCol.setCellValueFactory(new PropertyValueFactory<>("navbarTitle"));
        fileNameCol.setCellValueFactory(new PropertyValueFactory<>("fileName"));
        scriptCol.setCellValueFactory(new PropertyValueFactory<>("script"));
        // Display row data
        ObservableList<Pages> list = getUserList();
        table.setItems(list);
        table.getColumns().addAll(useCol, navbarTitleCol, fileNameCol, scriptCol);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.autosize();
        GridPane tableInsert = new GridPane();
        //tableInsert.getChildren().add(table);
        siteTemplate.getChildren().addAll(templateSectionHeader,templateSectionText,templateDir
                ,templateButton,sitePages,table);
        siteTemplate.setStyle("-fx-border-color: black;-fx-padding: 10;-fx-background-color: white");
        
        
        //VBox3
        GridPane style = new GridPane();
        Text styleHeader = new Text(props.getProperty(CourseSiteGeneratorProp.PAGE_STYLE.toString()));
        styleHeader.setFont(Font.font("Arial", FontWeight.NORMAL,15));
        Label schoolBanner = new Label(props.getProperty(CourseSiteGeneratorProp.BANNER_IMAGE.toString()));
        Label schoolBannerSelect = new Label("YALE UNIVERSITY");
        schoolBannerSelect.getStylesheets().add("https://fonts.googleapis.com/css?family=Lobster");
        schoolBannerSelect.setStyle("-fx-font-family: 'Lobster', cursive; -fx-font-size: 20;");
        
        Button schoolBannerButton = new Button(props.getProperty(CourseSiteGeneratorProp.CHANGE.toString()));
        style.add(styleHeader, 0, 0);
        style.add(schoolBanner,0,1);
        style.add(schoolBannerSelect,1,1);
        style.add(schoolBannerButton,2,1);
        
        Label leftFooterImg = new Label(props.getProperty(CourseSiteGeneratorProp.LEFT_FOOTER_IMAGE.toString()));
        Label leftFooterImgSelect = new Label("YALE UNIVERSITY");
        leftFooterImgSelect.getStylesheets().add("https://fonts.googleapis.com/css?family=Lobster");
        leftFooterImgSelect.setStyle("-fx-font-family: 'Lobster', cursive; -fx-font-size: 20;");
        Button leftFooterImgButton = new Button(props.getProperty(CourseSiteGeneratorProp.CHANGE.toString()));
        style.add(leftFooterImg,0,2);
        style.add(leftFooterImgSelect,1,2);
        style.add(leftFooterImgButton,2,2);
        
        
        Label rightFooterImg = new Label(props.getProperty(CourseSiteGeneratorProp.RIGHT_FOOTER_IMAGE.toString()));
        Label rightFooterSelect = new Label("YALE CS");
        rightFooterSelect.getStylesheets().add("https://fonts.googleapis.com/css?family=Lobster");
        rightFooterSelect.setStyle("-fx-font-family: 'Lobster', cursive; -fx-font-size: 20;");
        
        Button rightFooterButton = new Button(props.getProperty(CourseSiteGeneratorProp.CHANGE.toString()));
        style.add(rightFooterImg,0,3);
        style.add(rightFooterSelect,1,3);
        style.add(rightFooterButton,2,3);
        
        
        Label styleSheetLabel = new Label(props.getProperty(CourseSiteGeneratorProp.STYLESHEET.toString()));
        ComboBox styleSheetList = new ComboBox();
        styleSheetList.getItems().addAll(props.getProperty(CourseSiteGeneratorProp.STYLE_SELECTOR.toString()),"bmcc_style.css");
        styleSheetList.setValue(props.getProperty(CourseSiteGeneratorProp.STYLE_SELECTOR.toString()));
        
        
        style.add(styleSheetLabel,0,4);
        style.add(styleSheetList,1,4);
        
        Label note = new Label(props.getProperty(CourseSiteGeneratorProp.STYLE_NOTE.toString()));
        style.add(note,0,5,5,1);
        style.setStyle("-fx-border-color: black;-fx-padding: 10;-fx-background-color: white");
        
        csgDetailLayout.getChildren().add(courseInfoPane);
        
        csgDetailLayout.getChildren().add(siteTemplate);
        csgDetailLayout.getChildren().add(style);
        csgDetailLayout.setStyle("-fx-background-color: #3B5998; -fx-font-family: Courier New;");
        csgDetailLayout.setSpacing(10);
        
        setContent(csgDetailLayout);
        
        
    }

    private ObservableList<Pages> getUserList() {
        CheckBox cbHome = new CheckBox();
        cbHome.selectedProperty().setValue(Boolean.TRUE);
        Pages home = new Pages(cbHome, "Home", "index.html", "HomeBuilder.js");
        
        CheckBox cbSyllabus = new CheckBox();
        cbSyllabus.selectedProperty().setValue(Boolean.TRUE);
        Pages syllabus = new Pages(cbSyllabus, "Syllabus", "syllabus.html", "SyllabusBuilder.js");
        CheckBox cbSchedule = new CheckBox();
        
        cbSchedule.selectedProperty().setValue(Boolean.TRUE);
        Pages schedule = new Pages(cbSchedule, "Schedule", "schedule.html", "ScheduleBuilder.js");
        
        CheckBox cbHWs = new CheckBox();
        cbHWs.selectedProperty().setValue(Boolean.TRUE);
        Pages hws = new Pages(cbHWs, "HWs", "hws.html", "HWsBuilder.js");
        
        CheckBox cbProject = new CheckBox();
        cbProject.selectedProperty().setValue(Boolean.TRUE);
        Pages projects= new Pages(cbProject, "Projects", "projects.html", "ProjectsBuilder.js");
        
 
        ObservableList<Pages> list = FXCollections.observableArrayList(home, syllabus, schedule,hws,projects);
        return list;
    }

    public void resetWorkspace() {
        courseTitleText.setText("");
        instructorNameField.setText("");
        instructorHomeField.setText("");
    }

    public void reloadWorkspace(AppDataComponent dataComponent) {
        
    }
}
