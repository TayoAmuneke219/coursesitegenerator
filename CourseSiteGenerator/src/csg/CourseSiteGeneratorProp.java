package csg;

/**
 * This enum provides a list of all the user interface
 * text that needs to be loaded from the XML properties
 * file. By simply changing the XML file we could initialize
 * this application such that all UI controls are provided
 * in another language.
 * 
 * @author Richard McKenna & Tayo Amuneke
 * @version 1.0
 */
public enum CourseSiteGeneratorProp {
    // FOR SIMPLE OK/CANCEL DIALOG BOXES
    OK_PROMPT,
    CANCEL_PROMPT,
    
    // THESE ARE FOR TEXT PARTICULAR TO THE APP'S WORKSPACE CONTROLS
    TAS_HEADER_TEXT,
    NAME_COLUMN_TEXT,
    EMAIL_COLUMN_TEXT,
    NAME_PROMPT_TEXT,
    EMAIL_PROMPT_TEXT,
    START_HOUR_PROMPT_TEXT,
    END_HOUR_PROMPT_TEXT, 
    ADD_BUTTON_TEXT,
    CHANGE_TIME_BUTTON_TEXT, 
    UPDATE_TA_BUTTON_TEXT,
    CLEAR_BUTTON_TEXT,
    OFFICE_HOURS_SUBHEADER,
    OFFICE_HOURS_TABLE_HEADERS,
    DAYS_OF_WEEK,
    UPDATE_TIME_TITLE,
    UPDATE_TIME_MESSAGE,
    
    //Texts for Course Details workspace
    COURSE_DETAILS,
    COURSE_INFO,
    SUBJECT,
    CSE,
    ISE,
    NUMBER,
    DEMO219,
    FALL,
    SPRING,
    SEMESTER,
    YEAR,
    TITLE,
    TITLE_TEXT,
    INSTRUCTOR_NAME,
    INSTRUCTOR_NAME_TEXT,
    INSTRUCTOR_HOME,
    INSTRUCTOR_HOME_TEXT,
    EXPORT_DIR,
    EXPORT_DIR_TEXT,
    CHANGE,
    SITE_TEMPLATE,
    SITE_TEMPLATE_MESSAGE,
    SITE_TEMPLATE_DIR,
    SITE_TEMPLATE_BUTTON,
    SITE_PAGES,
    USE,
    NAVBAR_TITLE,
    FILE_NAME,
    SCRIPT,
    PAGE_STYLE,
    BANNER_IMAGE,
    LEFT_FOOTER_IMAGE,
    RIGHT_FOOTER_IMAGE,
    STYLESHEET,
    STYLE_NOTE,
    STYLE_SELECTOR,
    
    STYLE_SELECTOR_BMCC
        ,DASH
        ,UNDERGRAD
        

        ,RECITATIONS
        ,SECTION
        ,INSTRUCTOR
        ,DAY_TIME
        ,LOCATION
        ,TA
        ,R01
        ,MCKENNA
        ,DT
        ,TA_1
        ,TA_2
        ,ADD_UPDATE_BUTTON
        ,CLEAR_BUTTON	
        ,COLON
        
        
        ,SCHEDULE
        ,CALENDAR_BOUNDARIES
        ,STARTING	
        ,ENDING		
        ,SCHEDULE_ITEMS	
        ,TYPE		
        ,DATE			
        ,TOPIC		
        ,ADD_EDIT	
        ,TIME		
        ,SNOW_DAY	
        ,LINK		
        ,CRITERIA	
        
        
        
        ,PROJECTS	
        ,TEAMS		
        ,NAME		
        ,COLOR_HEX	
        ,TEXT_COLOR_HEX	
        ,COLOR		
        ,TEXT_COLOR	
        ,STUDENTS	
        ,FNAME		
        ,ROLE		
        ,LNAME		
        ,TEAM		
        ,FNAME_EX	
        ,C4_COMICS	
        ,DATA_DESIGNER	
        ,C4_COMIC_LINK,
    
    
        
        
        
    SCHEDULE_DATA,
    HOLIDAY,
    LECTURE,
    HW,
   // THESE ARE FOR ERROR MESSAGES PARTICULAR TO THE APP
    MISSING_TA_NAME_TITLE,
    MISSING_TA_NAME_MESSAGE,
    MISSING_TA_EMAIL_TITLE,
    MISSING_TA_EMAIL_MESSAGE,
    TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE,
    TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE,
    INVALID_TA_EMAIL_TITLE,
    INVALID_TA_EMAIL_MESSAGE,
    NO_UPDATE_TITLE,
    NO_UPDATE_MESSAGE,
    INVALID_TIME_INPUT_TITLE,
    INVALID_TIME_INPUT_MESSAGE, 
    
    RECITATION_DATA, ROOM_EX, SUPERVISING_TA, PROJECTS_DATA, C4_COMIC, TA_DATA
}
