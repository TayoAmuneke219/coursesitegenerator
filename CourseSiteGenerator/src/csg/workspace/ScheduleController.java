/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.data.Schedule;
import csg.data.ScheduleData;
import csg.tabs.ScheduleTab;
import java.time.LocalDate;
import javafx.scene.control.TableView;

/**
 *
 * @author jastworld
 */
public class ScheduleController {

    CourseSiteGeneratorApp app;

    ScheduleTab scheduleTab;
    public ScheduleController(CourseSiteGeneratorApp app) {
        this.app=app;
    }



    public void handleAddSchedule(ScheduleTab scheduleTab) {
        ScheduleData data = (ScheduleData) app.getScheduleData();
        this.scheduleTab = scheduleTab;
        String time = scheduleTab.getTimeField().getText();
        String type = scheduleTab.getTypeBox().getValue().toString();
        String title = scheduleTab.getTitleField().getText();
        String topic = scheduleTab.getTopicField().getText();
        String link = scheduleTab.getLinkField().getText();
        String criteria = scheduleTab.getCriteriaField().getText();
        LocalDate date = scheduleTab.getDatePicker().getValue();
        if(!time.isEmpty()&&!title.isEmpty()&&!topic.isEmpty()&&!link.isEmpty()&&!criteria.isEmpty()){
            data.addSchedule(type, date.toString(), time, title, topic, link, criteria);
            //data.addSchedule(type,date.toString(),title,topic);
            scheduleTab.resetFields();
        }
        else
            System.out.print("Got here");
        
    }

    public void handleDeleteRow(ScheduleTab scheduleTab) {
       ScheduleData data = (ScheduleData) app.getScheduleData();
       this.scheduleTab = scheduleTab;
       Schedule selectedItem = scheduleTab.getTable().getSelectionModel().getSelectedItem();
       try{
            System.out.print(selectedItem.getDate());
            data.removeSchedule(selectedItem.getTopic(), selectedItem.getTitle());
       }catch(NullPointerException e){
           //Select something
       }
    }
    
}
