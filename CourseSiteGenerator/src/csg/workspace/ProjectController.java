/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.data.ProjectData;
import csg.data.Students;
import csg.data.Teams;
import csg.tabs.ProjectTab;

/**
 *
 * @author jastworld
 */
public class ProjectController {
    CourseSiteGeneratorApp app;
    ProjectTab pTab;
    
    public ProjectController(CourseSiteGeneratorApp app){
        this.app = app;
    }
    
    public void handleDeleteTeam(ProjectTab aThis) {
        pTab =aThis;
        ProjectData pData = (ProjectData) app.getProjectData();
        Object selectedItem = pTab.getTeamTable().getSelectionModel().getSelectedItem();
                //.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                Teams team = (Teams) selectedItem;
                String teamName = team.getName();
                pData.removeTeams(teamName);
            }
        
        
        
    }

    public void handleAddTeam(ProjectTab aThis) {
        pTab =aThis;
        ProjectData pData = (ProjectData) app.getProjectData();
        
        String name = aThis.getNameField().getText();
        String color = pTab.getColorCPicker().getValue().toString();
        String textColor = pTab.getColorTPicker().getValue().toString();
        String link = pTab.getLinkField().getText().toString();
        if(!name.isEmpty()&&!link.isEmpty())
            pData.addTeams(name, color, textColor, link);
        else{
            //Use display to tell user it wasnt added
        }
    }

    public void handleDeleteStudent(ProjectTab aThis) {
        pTab =aThis;
        ProjectData pData = (ProjectData) app.getProjectData();
        Object selectedItem = pTab.getStudentTable().getSelectionModel().getSelectedItem();
                //.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                Students team = (Students) selectedItem;
                String teamName = team.getFirstName();
                
                pData.removeStudent(teamName,team.getLastName());
            }
    }

    public void handleAddStudent(ProjectTab aThis) {
        ProjectData pData = (ProjectData) app.getProjectData();
        String fName = aThis.getfNameField().getText();
        String lName = aThis.getlNameField().getText();
        String roleName = aThis.getRoleField().getText();
        String teamName = "";
        try{
            teamName = aThis.getTeamChoiceBox().getValue().toString();
        }catch(NullPointerException e){
        
        
        }
        if(!fName.isEmpty()&&!lName.isEmpty()&&!roleName.isEmpty()&&!teamName.isEmpty())
            pData.addStudent(fName, lName, lName, roleName);
        else{
            //Display an error
        }
    }

}
