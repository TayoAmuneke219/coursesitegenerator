/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.tabs.CourseDetailsTab;
import javafx.scene.control.TabPane;
import csg.CourseSiteGeneratorApp;
import csg.tabs.ProjectTab;
import csg.tabs.RecitationDataTab;
import csg.tabs.ScheduleTab;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

/**
 *
 * @author jastworld
 */
public class csgWorkspace extends AppWorkspaceComponent{
    
    
    CourseSiteGeneratorApp app;
    TabPane appMajor;
    TADataTab taData;
    CourseDetailsTab csgDetails;
    RecitationDataTab rdTab;
    ScheduleTab scheduleTab;
    ProjectTab projectTab;
    
    public csgWorkspace(CourseSiteGeneratorApp initApp){
        app = initApp;
        appMajor = new TabPane();
        
        
        taData = app.getTAData();
        
        
        taData.setClosable(false);
        csgDetails = new CourseDetailsTab(app);
        csgDetails.setClosable(false);
        
        rdTab = new RecitationDataTab(app);
        rdTab.setClosable(false);
        scheduleTab = new ScheduleTab(app);
        projectTab = new ProjectTab(app);
        
        appMajor.getTabs().addAll(csgDetails,taData,rdTab, scheduleTab,projectTab);
        
        workspace = new BorderPane();
        
        ((BorderPane) workspace).setCenter(appMajor);
        
    }

    @Override
    public void resetWorkspace() {
        taData.resetWorkspace();
        csgDetails.resetWorkspace();
        projectTab.resetWorkspace();
        scheduleTab.resetFields();
        rdTab.resetWorkspace();
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        taData.reloadWorkspace(dataComponent);
       
    }
    
    public TADataTab getTADataTab(){
        return taData;
    }
    
    public ScheduleTab getScheduletab(){
        return scheduleTab;
    }
    
}
