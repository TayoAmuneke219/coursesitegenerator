/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CourseSiteGeneratorApp;
import djf.components.AppDataComponent;
import java.util.Collections;
import java.util.Comparator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Tayo Amuneke
 */
public class ProjectData implements AppDataComponent{

    
    // WE'LL NEED ACCESS TO THE APP TO NOTIFY THE GUI WHEN DATA CHANGES
    CourseSiteGeneratorApp app;

    // NOTE THAT THIS DATA STRUCTURE WILL DIRECTLY STORE THE
    // DATA IN THE ROWS OF THE TABLE VIEW
    ObservableList<Students> students;
    
    ObservableList<Teams> teams;
    
    public ProjectData(CourseSiteGeneratorApp initApp){
            app=initApp;
            
              // CONSTRUCT THE LIST OF TAs FOR THE TABLE
            students = FXCollections.observableArrayList();
            teams = FXCollections.observableArrayList();
    }

    @Override
    public void resetData() {

    }
    
     public void addTeams(String name, String color, String textColor, String link) {
        // MAKE THE TA
        Teams team = new Teams(name,color,textColor,link);

        // ADD THE TA
        if (!containsTeam(name, color,link)) {
            teams.add(team);
        }

        // SORT THE TAS
        Collections.sort(teams,
                 new Comparator<Teams>()
                 {
                     public int compare(Teams f1, Teams f2)
                     {
                         return f1.getName().toString().compareTo(f2.getName().toString());
                     }        
                 });
    }

    public void removeTeams(String name) {
        for (Teams team : teams) {
            if (name.equals(team.getName())) {
                teams.remove(team);
                return;
            }
        }
    }
    
     public boolean containsTeam(String name, String color, String link) {
        for (Teams team : teams) {
            if (team.getName().equals(name)) {
                return true;
            }
            if (team.getColor().equals(color)) {
                return true;
            }
             if (team.getLink().equals(link)) {
                return true;
            }
        }
        return false;
    }


    public ObservableList<Teams> getTeamList() {
        return teams;
    }
    
    
    //////////////////////////Students///////////////////////////
    public void addStudent(String firstName, String lastName, String team, String role) {
        // MAKE THE TA
        Students student = new Students(firstName,lastName,team,role);

        // ADD THE TA
        if (!containsStudent(firstName,lastName,team,role)) {
            students.add(student);
        }

        // SORT THE TAS
        Collections.sort(students,
                 new Comparator<Students>()
                 {
                     public int compare(Students f1, Students f2)
                     {
                         return f1.getFirstName().compareTo(f2.getFirstName());
                     }        
                 });
    }

    public void removeStudent(String firstName, String lastName) {
        for (Students stu : students) {
            if (firstName.equals(stu.getFirstName())&&lastName.equals(stu.getLastName())) {
                students.remove(stu);
                return;
            }
        }
    }
    
     public boolean containsStudent(String firstName, String lastName,String team, String role) {
        for (Students stu : students) {
            if (stu.getFirstName().equals(firstName)) {
                return true;
            }
            if (stu.getLastName().equals(lastName)) {
                return true;
            }
            if (stu.getTeam().equals(team)) {
                return true;
            }
            if (stu.getRole().equals(role)) {
                return true;
            }
        }
        return false;
    }


    public ObservableList<Students> getStudents() {
        return students;
    }
    

    
}
