/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.scene.control.CheckBox;

/**
 *
 * @author Tayo Amuneke
 */
public class Recitation {
   
   private String section;
   private String instructor;
   private String dayTime;
   private String location;
   private String ta,ta2;
 
   public Recitation(String section, String instructor, String dayTime, String location,
           String ta,String ta2) {
       this.section=section;
       this.instructor=instructor;
       this.dayTime=dayTime;
       this.location=location;
       this.ta= ta;
       this.ta2=ta2;
   }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getInstructor() {
        return instructor;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    public String getDayTime() {
        return dayTime;
    }

    public void setDayTime(String dayTime) {
        this.dayTime = dayTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTa() {
        return ta;
    }

    public void setTa(String ta) {
        this.ta = ta;
    }

    public String getTa2() {
        return ta2;
    }

    public void setTa2(String ta2) {
        this.ta2 = ta2;
    }
   
   
 
}
