/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CourseSiteGeneratorApp;
import djf.components.AppDataComponent;
import java.util.Collections;

import java.util.Comparator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Tayo Amuneke
 */
public class ScheduleData implements AppDataComponent{

    
    // WE'LL NEED ACCESS TO THE APP TO NOTIFY THE GUI WHEN DATA CHANGES
    CourseSiteGeneratorApp app;

    // NOTE THAT THIS DATA STRUCTURE WILL DIRECTLY STORE THE
    // DATA IN THE ROWS OF THE TABLE VIEW
    ObservableList<Schedule> schedule;
    
    public ScheduleData(CourseSiteGeneratorApp initApp){
            app=initApp;
            
              // CONSTRUCT THE LIST OF TAs FOR THE TABLE
            schedule = FXCollections.observableArrayList();
    }
    
    @Override
    public void resetData() {
        
    }
    
    public void addSchedule(String type, String date, String time, String title, String topic, String link, String criteria) {
        // MAKE THE TA
        Schedule scheduler = new Schedule(type,date,time,title,topic,link,criteria);

        // ADD THE TA
        if (!containsSchedule(topic, title)) {
            schedule.add(scheduler);
        }

        // SORT THE TAS
        Collections.sort(schedule,
                 new Comparator<Schedule>()
                 {
                     public int compare(Schedule f1, Schedule f2)
                     {
                         return f1.getTitle().toString().compareTo(f2.getTitle().toString());
                     }        
                 });
    }

    public void removeSchedule(String topic, String title) {
        for (Schedule sch : schedule) {
            if (topic.equals(sch.getTopic())&&title.equals(sch.getTitle())) {
                schedule.remove(sch);
                return;
            }
        }
    }
    
     public boolean containsSchedule(String topic, String title) {
        for (Schedule sch : schedule) {
            if (sch.getTopic().equals(topic)) {
                return true;
            }
            if (sch.getTitle().equals(title)) {
                return true;
            }
        }
        return false;
    }


    public ObservableList<Schedule> getScheduleList() {
        return schedule;
    }
    
}
