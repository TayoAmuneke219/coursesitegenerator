/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.scene.control.CheckBox;

/**
 *
 * @author Tayo Amuneke
 */
public class Pages {
     
   private CheckBox use;
   
   private String navbar_title;
   private String file_name;
   private String script;
 
   public Pages(CheckBox use, String navbar_title, String file_name, String script) {
       this.use = use;
       this.navbar_title = navbar_title;
       this.file_name = file_name;
       this.script = script;
   }
 
   public CheckBox getCheckBox() {
       return use;
   }
 
   public void setCheckBox(CheckBox use) {
       this.use = use;
   }
 
   public String getNavbarTitle() {
       return navbar_title;
   }
 
   public void setNavbarTitle(String navbar_title) {
       this.navbar_title = navbar_title;
   }
 
   public String getFileName() {
       return file_name;
   }
 
   public void setFileName(String file_name) {
       this.file_name = file_name;
   }
 
   public String getScript() {
       return script;
   }
 
   public void setScript(String script) {
       this.script = script;
   }
 
}
